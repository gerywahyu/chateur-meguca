from chessboard.ChessPieces import Colour
from chessboard import ChessImpl
import itertools
import random


class ChessBoard:
    """A representation of a chessboard."""

    _all_locations = list(itertools.product(range(8), range(8)))

    def __init__(self, pieces):
        """Create a chessboard out of a collection of Pieces.
        
        Each piece must have a unique location.
        """
        self._pieces = tuple(p for p in pieces)
        self._view = ChessImpl.make_view(self._pieces)
        self._eval_result = None
        self._score = None

    def __repr__(self):
        return 'ChessBoard(%s)' % repr(self._pieces)

    @classmethod
    def create_random(cls, piece_specs):
        """Create a random chessboard from a list of piece specifications.
        
        Example:
        ```
        create_random([(Colour.white, Queen, 2), (Colour.white, Bishop, 2),
                       (Colour.white, Rook, 2), (Colour.white, Queen, 2),
                       (Colour.black, Knight, 1), (Colour.black, Queen, 2)])
        ```
        """
        total = sum(map(lambda spec: spec[2], piece_specs))
        coords = random.sample(cls._all_locations, total)
        pieces = []
        j = 0
        for (colour, ctor, n) in piece_specs:
            for i in range(n):
                x, y = coords[j]
                pieces.append(ctor(x, y, colour))
                j += 1
        return cls(pieces)

    @property
    def pieces(self):
        """Returns a tuple containing all pieces on this board.
        
        DO NOT modify the `ChessPiece`s returned.
        """
        return self._pieces

    def as_grid(self, space=''):
        grid = [['.'] * 8 for _ in range(8)]
        for piece in self._pieces:
            grid[7 - piece.y][piece.x] = piece.symbol
        return '\n'.join(space.join(row) for row in grid)

    def free_spaces(self):
        """Return a set of free (unoccupied) squares on the board."""
        return ChessImpl.free_spaces(self._view)

    def random_loc(self):
        """Return a random unoccupied location on the board, or None."""
        spaces = self.free_spaces()
        if not spaces:
            return None
        return ChessImpl.to_point(random.choice(spaces))

    def evaluate(self):
        """Count the number of attacking pairs of the same side and of
        opposite sides.
        
        Returns a tuple `(same_side_attacks, opposite_side_attacks)`
        """
        if self._eval_result is not None:
            return self._eval_result
        own_side, opp_side = 0, 0
        for p in self._pieces:
            w, b = p.attack(self._view)
            if p.c == Colour.white:
                own_side += w
                opp_side += b
            else:
                own_side += b
                opp_side += w
        self._eval_result = (own_side, opp_side)
        n_white = sum(1 if p.c == Colour.white else 0 for p in self._pieces)
        n_black = len(self._pieces) - n_white
        same_side_pairs = n_white * (n_white - 1) + n_black * (n_black - 1)
        self._score = 420 * (same_side_pairs - own_side) + opp_side
        return self._eval_result

    def score(self):
        """Evaluate the board and return an 18-bit score. Higher scores are
        better.
        """
        if self._score is None:
            self.evaluate()
        return self._score

    def free_neighbours(self, x, y):
        """Return the free neighbours of the square at (x, y)."""
        spaces = self.free_spaces()
        for dx in range(-1, 2):
            for dy in range(-1, 2):
                x1, y1 = x + dx, y + dy
                if ChessImpl.valid_point(x1, y1) and \
                   ChessImpl.board_index(x1, y1) in spaces:
                    yield x1, y1

    def free_blocks(self):
        """Return the free spaces."""
        spaces = self.free_spaces()
        for space in spaces:
            yield ChessImpl.to_point(space)

    def move_piece(self, piece, new_x, new_y):
        """Return a new ChessBoard with the specified ChessPiece moved to
        (new_x, new_y). The original is not modified.
        """
        pn = type(piece)(new_x, new_y, piece.c)
        return self.__class__(p if p is not piece else pn for p in self._pieces)

    def splice(self, other, crossover_point):
        h1 = self.pieces[:crossover_point]
        h2 = other.pieces[crossover_point:]
        s1 = set(ChessImpl.board_index(p.x, p.y) for p in h1)
        s2 = set(ChessImpl.board_index(p.x, p.y) for p in h2)
        conflicts = s1 & s2
        union = s1 | s2
        free = {pos for pos in range(64) if pos not in union}
        pieces = list(h1)
        for p in h2:
            pos = ChessImpl.board_index(p.x, p.y)
            if pos in conflicts:
                pos = random.choice(list(free))
                free.discard(pos)
            x, y = ChessImpl.to_point(pos)
            pieces.append(p.__class__(x, y, p.c))
        return self.__class__(pieces)
