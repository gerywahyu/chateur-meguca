"""Implementation of chessboard calculations. Here be dragons."""

# import intbitset
import collections
from chessboard import ChessPieces


# Indices and coordinates

def board_index(x, y):
    return (y << 3) | x


def to_point(val):
    return val % 8, val >> 3


def valid_point(x, y):
    return (0 <= x < 8) and (0 <= y < 8)


all_locations = set(range(64))

# Create views

ChessView = collections.namedtuple('ChessView', 'all sw sb')


def make_view(pieces):
    sw = set()
    sb = set()
    for p in pieces:
        pos = board_index(p.x, p.y)
        if p.c == ChessPieces.Colour.white:
            sw.add(pos)
        else:
            sb.add(pos)
    return ChessView(sw | sb, sw, sb)


def print_setboard(s):
    for off in range(56, -1, -8):
        print(' '.join('X' if i in s else '.' for i in range(off, off + 8)))


def free_spaces(v):
    return list(all_locations - v.all)


# Attack masks

rank_masks = [set(range(y8, y8 + 8)) for y8 in range(0, 64, 8)]


def rank_mask(_, y):
    return rank_masks[y]


file_masks = [set(range(x, 64, 8)) for x in range(8)]


def file_mask(x, _):
    return file_masks[x]


diagonal_masks = []
for d in range(-7, 0):
    diagonal_masks.append(set(range(-d, (d + 8) << 3, 9)))
for d in range(8):
    diagonal_masks.append(set(range(d << 3, 64, 9)))


def diagonal_mask(x, y):
    diagonal = y - x
    return diagonal_masks[diagonal + 7]


adiagonal_masks = []
for s in range(8):
    adiagonal_masks.append(set(range(s, (s << 3) + 1, 7)))
for s in range(8, 15):
    adiagonal_masks.append(set(range(((s - 6) << 3) - 1, 64, 7)))


def adiagonal_mask(x, y):
    s = x + y
    return adiagonal_masks[s]


# Calculate attacks

def sum_attacks(v, x, y, mask_fns):
    """Calculate `(white_pieces_attacked, black_pieces_attacked)`"""
    occ = v.all
    pos = board_index(x, y)
    aw, ab = 0, 0
    for mask_fn in mask_fns:
        up, dn = 64, -1
        for i in mask_fn(x, y):
            if i not in occ:
                continue
            if pos < i < up:
                up = i
            if pos > i > dn:
                dn = i
        if up < 64:
            aw += int(up in v.sw)
            ab += int(up in v.sb)
        if dn > -1:
            aw += int(dn in v.sw)
            ab += int(dn in v.sb)
    return aw, ab


# Piece attacks

def attack_queen(v, x, y):
    return sum_attacks(v, x, y,
                       (rank_mask, file_mask, diagonal_mask, adiagonal_mask))


def attack_bishop(v, x, y):
    return sum_attacks(v, x, y, (diagonal_mask, adiagonal_mask))


def attack_rook(v, x, y):
    return sum_attacks(v, x, y, (rank_mask, file_mask))


def attack_knight(v, x, y):
    aw, ab = 0, 0
    for i in (-1, 1):
        for j in (-2, 2):
            x1, y1 = x + i, y + j
            x2, y2 = x + j, y + i
            if valid_point(x1, y1):
                c = board_index(x1, y1)
                aw += int(c in v.sw)
                ab += int(c in v.sb)
            if valid_point(x2, y2):
                c = board_index(x2, y2)
                aw += int(c in v.sw)
                ab += int(c in v.sb)
    return aw, ab
