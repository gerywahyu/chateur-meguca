from chessboard.ChessPieces import Bishop, Queen, Knight, Rook, Colour
from solver.algorithm.Annealing import Annealing


def test_batch():
    specs = [(Colour.white, Queen, 2), (Colour.white, Bishop, 2), (Colour.white, Rook, 2), (Colour.white, Knight, 2),
             (Colour.black, Queen, 2), (Colour.black, Bishop, 2), (Colour.black, Rook, 2), (Colour.black, Knight, 2)]
    solver = Annealing(1000, 0.5, 50)
    results = solver.solve(10, 1000, specs)
    for result in results:
        print(result)
        print()


test_batch()
