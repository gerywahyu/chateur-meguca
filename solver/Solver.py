from abc import ABC, abstractmethod


class Solver(ABC):

    @abstractmethod
    def run_batch(self, piece_specs, iterations):
        return None

    def solve(self, batches, iters_per_batch, piece_specs):
        results = [self.run_batch(piece_specs, iters_per_batch) for i in range(batches)]
        return results
