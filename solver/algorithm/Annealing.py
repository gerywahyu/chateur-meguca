from solver.algorithm.LocalSearch import LocalSearch
import random
import math


class Annealing(LocalSearch):

    stop_delay = 132

    def __init__(self, temp, rate, maintain):
        self._init_temp = temp
        self._temp = temp
        self._rate = rate
        self._maintain = maintain

    @staticmethod
    def boltzmann_distribution(eval, eval_i, temperature):
        if temperature == 0:
            return 0.0
        return math.exp(-(eval - eval_i) / temperature)

    @staticmethod
    def get_random_piece(state):
        pieces = state.pieces
        chosen_index = random.randint(0, len(pieces)-1)
        return pieces[chosen_index]

    @staticmethod
    def get_random_state(random_piece, state):
        random_states = list(LocalSearch.generate_piece_states(random_piece, state))
        return random.choice(random_states)

    def step(self, curr_state, iter_num, improve):
        random_piece = self.get_random_piece(curr_state)
        next_state = self.get_random_state(random_piece, curr_state)
        diff_score = next_state.score() - curr_state.score()

        if diff_score >= 0:
            curr_state = next_state
            improve += 1
        else:
            prob = self.boltzmann_distribution(
                curr_state.score(),
                next_state.score(),
                self._temp
            )
            if prob > random.random():
                improve += 1
                curr_state = next_state
        if iter_num >= self._maintain:
            self._temp *= (1 - self._rate)
        return curr_state, improve

    def run_batch(self, state, iterations):
        self._temp = self._init_temp
        return super().run_batch(state, iterations)
