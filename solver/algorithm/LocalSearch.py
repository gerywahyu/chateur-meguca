from abc import abstractmethod
from chessboard.ChessBoard import ChessBoard
from solver.Solver import Solver
import time


class LocalSearch(Solver):

    @staticmethod
    def generate_piece_states(piece, board):
        points = board.free_blocks()
        for x, y in points:
            yield board.move_piece(piece, x, y)

    @abstractmethod
    def step(self, state, iter_num, improve):
        return state, improve

    stop_delay = 0

    def run_batch(self, piece_specs, iterations):
        state = ChessBoard.create_random(piece_specs)
        improve = 0
        metrics = [state.evaluate()]
        best_score = state.score()
        best_iter = 0

        start_time = time.time()
        i = 0
        for i in range(1, iterations + 1):
            state, improve = self.step(state, i, improve)
            metrics.append(state.evaluate())
            if state.evaluate()[0] == 0:
                if best_score < state.score():
                    best_score = state.score()
                    best_iter = i
                elif (i - best_iter) >= self.stop_delay:
                    break
        end_time = time.time()

        result = {
            "steps": i,
            "improvement": improve,
            "state": state,
            "total_time": end_time - start_time
        }
        return result
