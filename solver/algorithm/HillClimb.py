from solver.algorithm.LocalSearch import LocalSearch


class HillClimb(LocalSearch):

    stop_delay = 5

    def step(self, state, iter_num, improve):
        piece = state.pieces[iter_num % len(state.pieces)]
        neighbour_states = LocalSearch.generate_piece_states(piece, state)
        curr_eval_result, curr_score = state.evaluate(), state.score()
        for neighbour in neighbour_states:
            if curr_eval_result[0] == 0:
                break
            eval_result, score = neighbour.evaluate(), neighbour.score()
            if curr_score < score:
                improve += 1
                curr_score = score
                curr_eval_result = eval_result
                state = neighbour
        return state, improve
