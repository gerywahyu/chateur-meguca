from solver.Solver import Solver
from chessboard.ChessBoard import ChessBoard
import itertools
import random
import time
import math


def weighted_choice(population, cum_weights):
    rand = random.random()
    i = 0
    for i, weight in enumerate(cum_weights):
        if weight > rand:
            break
    return population[i]


class Genetic(Solver):

    def __init__(self, population=1000, p_mutation=0.01, cull_factor=0.3):
        if population % 2 != 0:
            raise ValueError('population must be even')
        self.population = population
        self.p_mutation = p_mutation
        self.cull_pop = int(population * cull_factor)

    def cull(self, population):
        population.sort(key=lambda b: b.score())
        del population[:self.cull_pop]
        return population

    def generate_states(self, pop, piece_specs):
        states = [ChessBoard.create_random(piece_specs) for i in range(pop)]
        return self.cull(states)

    @staticmethod
    def assess_population(states):
        scores = [math.exp(state.score()/50000) for state in states]
        total = sum(scores)
        avg = total / len(scores)
        return scores, total, avg

    def cross(self, p1, p2):
        crossover_point = random.randint(0, len(p1.pieces))
        f1 = p1.splice(p2, crossover_point)
        f2 = p2.splice(p1, crossover_point)
        return f1, f2

    @staticmethod
    def mutate(state):
        piece = random.choice(state.pieces)
        x, y = state.random_loc()
        return state.move_piece(piece, x, y)

    def step(self, states, scores, total, avg, improvement):
        weights = [score / total for score in scores]
        cum_weights = list(itertools.accumulate(weights))
        offspring = []
        for i in range(self.population // 2):
            p1 = weighted_choice(states, cum_weights)
            p2 = weighted_choice(states, cum_weights)
            f1, f2 = self.cross(p1, p2)
            if random.random() < self.p_mutation:
                f1 = self.mutate(f1)
                f2 = self.mutate(f2)
            offspring.append(f1)
            offspring.append(f2)
        self.cull(offspring)
        scores, total, new_avg = self.assess_population(offspring)
        if new_avg > avg:
            improvement += 1
        return offspring, scores, total, new_avg, improvement

    def run_batch(self, piece_specs, iterations):
        states = self.generate_states(self.population, piece_specs)
        max_state = None
        scores, total, avg = self.assess_population(states)
        improve = 0
        metrics = [(max(scores), avg, min(scores))]

        start_time = time.time()
        i = 0
        for i in range(1, iterations + 1):
            states, scores, total, avg, improve = \
                self.step(states, scores, total, avg, improve)
            metrics.append((max(scores), avg, min(scores)))
            max_i, max_score = max(enumerate(scores), key=lambda x: x[1])
            max_state = states[max_i]
            if max_state.evaluate()[0] == 0:
                break
        end_time = time.time()

        result = {
            "steps": i,
            "improvement": improve,
            "state": max_state,
            "total_time": end_time - start_time
        }
        return result
