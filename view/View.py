from view.Presenter import Presenter
from view.Presenter import Algorithm
from view.ChessUI import ChessUI
import sys
import os


class View:

    def __init__(self):
        self.presenter = Presenter()
    
    @staticmethod
    def clear_screen():
        if os.name == 'nt': 
            _ = os.system('cls') 
        else: 
            _ = os.system('clear')

    @staticmethod
    def print_logo():
        View.clear_screen()
        print()
        print("                                            ,----,                                             ")
        print("         ,--.                             ,/   .`|       ,--,                 ,--.             ")
        print("       ,--.'|                           ,`   .'  :     ,--.'|   ,---,       ,--.'|  ,----..    ")
        print("   ,--,:  : |                  ,---,  ;    ;     /  ,--,  | :,`--.' |   ,--,:  : | /   /   \   ")
        print(",`--.'`|  ' :    ,---,.       /_ ./|.'___,/    ,',---.'|  : '|   :  :,`--.'`|  ' :|   :     :  ")
        print("|   :  :  | |  ,'  .' | ,---, |  ' :|    :     | |   | : _' |:   |  '|   :  :  | |.   |  ;. /  ")
        print(":   |   \ | :,---.'   ,/___/ \.  : |;    |.';  ; :   : |.'  ||   :  |:   |   \ | :.   ; /--`   ")
        print("|   : '  '; ||   |    | .  \  \ ,' '`----'  |  | |   ' '  ; :'   '  ;|   : '  '; |;   | ;  __  ")
        print("'   ' ;.    ;:   :  .'   \  ;  `  ,'    '   :  ; '   |  .'. ||   |  |'   ' ;.    ;|   : |.' .' ")
        print("|   | | \   |:   |.'      \  \    '     |   |  ' |   | :  | ''   :  ;|   | | \   |.   | '_.' : ")
        print("'   : |  ; .'`---'         '  \   |     '   :  | '   : |  : ;|   |  ''   : |  ; .''   ; : \  | ")
        print("|   | '`--'                 \  ;  ;     ;   |.'  |   | '  ,/ '   :  ||   | '`--'  '   | '/  .' ")
        print("'   : |                      :  \  \    '---'    ;   : ;--'  ;   |.' '   : |      |   :    /   ")
        print(";   |.'                       \  ' ;             |   ,/      '---'   ;   |.'       \   \ .'    ")
        print("'---'                          `--`              '---'               '---'          `---`      ")
        print()

    @staticmethod
    def show_solving_algorithm_choice():
        print("Please Choose the Algorithm")
        print("1. Hill Climbing")
        print("2. Simulated Annealing")
        print("3. Genetic")
        print()

    @staticmethod
    def get_local_search_config():
        batches = int(input("Number of batches: "))
        iters_per_batch = int(input("Number of iterations per batch: "))
        return batches, iters_per_batch

    @staticmethod
    def get_simulated_annealing_config():
        temp = int(input("Starting temperature: "))
        rate = float(input("Temperature rate of descent (0-1): "))
        maintain = int(input("Maintain temperature for: "))
        return temp, rate, maintain
    
    @staticmethod
    def get_genetic_config():
        population = int(input("Population amount: "))
        p_mutation = float(input("Mutation probability: "))
        cull_factor = float(input("Culling: "))
        return population, p_mutation, cull_factor

    def get_solving_algorithm_choice(self):
        self.show_solving_algorithm_choice()
        algo_choice = int(input("Algorithm's choice: "))
        print()
        return algo_choice

    def get_pieces_spec(self, file_name):
        try:
            f = open(file_name, "r")
            for line in f:
                data = line.replace("\n", "").split(" ")
                self.presenter.add_piece(data[0].lower(), data[1].lower(), int(data[2]))
        except FileNotFoundError:
            print("File not found!")
            sys.exit(1)

    def read_file(self):
        file_name = input("Please input file name: ")
        print()
        self.get_pieces_spec(file_name)

    @staticmethod
    def print_board(board, space=''):
        grid = [['.'] * 8 for _ in range(8)]
        for piece in board.pieces:
            grid[7 - piece.y][piece.x] = piece.symbol
        return '\n'.join(space.join(row) for row in grid)

    @staticmethod
    def print_result(result):
        for i, res in enumerate(result):
            evaluate = res['state'].evaluate()
            print("=================================")
            print("Batch", i)
            ChessUI.print_board(res['state'])
            print("Steps:", res['steps'])
            print("Improvement:", res['improvement'])
            print("Time:", res['total_time'], "second")
            print("Same side:", evaluate[0])
            print("Opponent side:", evaluate[1])
            print("=================================", '\n\n')

    def run(self):
        View.print_logo()
        self.read_file()
        algo_choice = self.get_solving_algorithm_choice()

        batches, iters_per_batch = self.get_local_search_config()
        if algo_choice == Algorithm.HILL_CLIMBING.value or algo_choice == Algorithm.SIMULATED_ANNEALING.value:
            if algo_choice == Algorithm.SIMULATED_ANNEALING.value:
                temp, rate, maintain = self.get_simulated_annealing_config()
                self.clear_screen()
                print("Sit down and chill, while we're doing the dirty job for you.\nLoading...", end="")
                result = self.presenter.solve_by_annealing(temp, rate, batches, iters_per_batch, maintain)
            else:
                self.clear_screen()
                print("Sit down and chill, while we're doing the dirty job for you.\nLoading...", end="")
                result = self.presenter.solve_by_hill_climbing(batches, iters_per_batch)
        else:
            self.clear_screen()
            print("Chill. This solver takes quite a long time.\nLoading...", end="")
            result = self.presenter.solve_by_genetic(batches, iters_per_batch)
        self.clear_screen()
        self.print_result(result)
