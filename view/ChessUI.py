from enum import Enum


class PieceCode(Enum):
    WHITE_QUEEN = '♛'
    WHITE_ROOK = '♜'
    WHITE_BISHOP = '♝'
    WHITE_KNIGHT = '♞'
    BLACK_QUEEN = '♕'
    BLACK_ROOK = '♖'
    BLACK_BISHOP = '♗'
    BLACK_KNIGHT = '♘'

class ChessUI:

    @staticmethod
    def get_chess(code):
        if code == 'q': return PieceCode.BLACK_QUEEN.value
        elif code == 'k': return PieceCode.BLACK_KNIGHT.value
        elif code == 'b': return PieceCode.BLACK_BISHOP.value
        elif code == 'r': return PieceCode.BLACK_ROOK.value
        elif code == 'Q': return PieceCode.WHITE_QUEEN.value
        elif code == 'K': return PieceCode.WHITE_KNIGHT.value
        elif code == 'B': return PieceCode.WHITE_BISHOP.value
        elif code == 'R': return PieceCode.WHITE_ROOK.value

    @staticmethod
    def init_grid():
        # Init square border constants
        allbox = u''.join(chr(9472 + x) for x in range(200))
        box = [ allbox[i] for i in (2, 0, 12, 16, 20, 24, 44, 52, 28, 36, 60) ]
        (vbar, hbar, ul, ur, ll, lr, nt, st, wt, et, plus) = box
        h3 = hbar * 3

        grid = [['' for _ in range(34)] for __ in range(17)]
        for i in range(17):
            for j in range(34):
                if(i==0 and j==0): grid[i][j] = ul
                elif(i==0 and j==32): grid[i][j] = ur
                elif(i==16 and j==0): grid[i][j] = ll
                elif(i==16 and j==32): grid[i][j] = lr
                elif(i%2==0 and j==0): grid[i][j] = wt
                elif(i%2==0 and j==32): grid[i][j] = et
                elif(i==0 and j%4==0): grid[i][j] = nt
                elif(i%2==0 and j%4==0 and i<16): grid[i][j] = plus
                elif(i==16 and j%4==0): grid[i][j] = st
                elif(i%2==1 and j%4>=1): grid[i][j] = ' '
                elif(i%2==1 and j%4==0): grid[i][j] = vbar
                else: grid[i][j] = hbar
        return grid

    @staticmethod
    def print_board(board):
        grid = ChessUI.init_grid()
        for piece in board._pieces:
            loc_y = (7-piece.y)*2 + 1
            loc_x = piece.x*4 + 2
            grid[loc_y][loc_x] = ChessUI.get_chess(piece.symbol)
        
        for i in range(17):
            for j in range(33):
                print(grid[i][j], end="")
            print()