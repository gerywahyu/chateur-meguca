from solver.algorithm.HillClimb import HillClimb
from solver.algorithm.Annealing import Annealing
from solver.algorithm.Genetic import Genetic
from chessboard import ChessPieces
from enum import Enum


class Algorithm(Enum):
    HILL_CLIMBING = 1
    SIMULATED_ANNEALING = 2
    GENETIC = 3

class Presenter:

    pieces_type = {
        "queen": ChessPieces.Queen,
        "knight": ChessPieces.Knight,
        "bishop": ChessPieces.Bishop,
        "rook": ChessPieces.Rook
    }

    pieces_color = {
        "white": ChessPieces.Colour.white,
        "black": ChessPieces.Colour.black
    }

    def __init__(self):
        self.pieces_spec = []

    def add_piece(self, piece_color, piece_type, total_pieces):
        try:
            color = self.pieces_color[piece_color]
            piece = self.pieces_type[piece_type]
            self.pieces_spec.append((color, piece, total_pieces))
        except KeyError:
            raise ValueError("Input Error")

    def solve_by_hill_climbing(self, total_batch, iteration):
        if len(self.pieces_spec) == 0:
            raise ValueError("Please add piece first!")
        hc = HillClimb()
        return hc.solve(total_batch, iteration, self.pieces_spec)

    def solve_by_annealing(self, temp, rate, total_batch, iteration, maintain):
        if len(self.pieces_spec) == 0:
            raise ValueError("Please add piece first!")
        sa = Annealing(temp, rate, maintain)
        return sa.solve(total_batch, iteration, self.pieces_spec)

    def solve_by_genetic(self, total_batch, iteration):
        if len(self.pieces_spec) == 0:
            raise ValueError("Please add piece first!")
        sa = Genetic()
        return sa.solve(total_batch, iteration, self.pieces_spec)
